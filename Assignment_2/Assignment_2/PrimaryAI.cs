﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2 {

    // Number of moves ahead that the AI plans
    public enum Difficulty {
        Noob = 1,
        Beginner = 2,
        Novice = 3,
        Practiced = 4,
        Intermediate = 5,
        Advanced = 6,
        Professional = 7,
        Expert = 8,
        Master = 9,
        God = Int32.MaxValue
    }

    /// <summary>
    /// Main AI class.
    /// <para> The idea behind this class is that once you give it a game board and team, 
    /// it stays as a part of that game until reasigned to another game. </para>
    /// <para> To do this, initialize it, and then use the following: </para>
    /// <para> To have the AI join a new game, use the JoinGame method and pass in the
    /// GameBoard and team as parameters. </para>
    /// <para> To have the AI leave a game, use the LeaveGame method. </para>
    /// <para> While the AI is in a game, simply call ChooseNextMove to get the AI's next
    /// move. </para>
    /// </summary>
    public class PrimaryAI : GamePlayerInterface {
        private GameBoard _gameBoard;
        private Team _myTeam;
        private StateTree minMaxTree;
        private int maxDepth;
        private string report;

        /// <summary>
        /// Constructor
        /// <para> Initializes the PrimaryAI without having it join a game. </para>
        /// </summary>
        public PrimaryAI() {
            this._gameBoard = null;
            this._myTeam = Team.None;
            this.minMaxTree = null;
            this.maxDepth = 2;
            this.report = "";
        }

        /// <summary>
        /// <para> Has the AI leave the game, and remove its information of that game. </para>
        /// </summary>
        public void LeaveGame() {
            this._gameBoard = null;
            this._myTeam = Team.None;
            this.minMaxTree = null;
        }

        /// <summary>
        /// <para> Has the AI join a game given by the parameters. </para>
        /// </summary>
        /// <param name="boardOfGameToJoin"> The board of the game for the AI to join. </param>
        /// <param name="teamToJoin"> The team that the AI will play. </param>
        /// <param name="difficulty"></param>
        public void JoinGame(GameBoard boardOfGameToJoin, Team teamToJoin) {
            if (Team.None != this._myTeam) {
                // AI is already in game.
                return;
            }
            this._gameBoard = boardOfGameToJoin;
            this._myTeam = teamToJoin;
            this.minMaxTree = null;
        }

        /// <summary>
        /// <para> Has the AI choose a next move based on the game it is currently in. </para>
        /// </summary>
        /// <para> Return: </para>
        /// <para> If the AI is in a game, returns a string containing AI's chosen move. </para>
        /// <para> Else returns a blank string. </para>
        /// <returns> String containing chosen move if in a game, blank string if
        /// AI is not in a game. </returns>
        public Move ChooseNextMove() {
            System.Diagnostics.Stopwatch thoughtTimer = new System.Diagnostics.Stopwatch();
            Console.Write("AI: 'Okay, let me think about my move.'" + Environment.NewLine);
            thoughtTimer.Start();
            int numExplored = 0;

            this.minMaxTree = new StateTree(new State(this._gameBoard, this._myTeam));
            this.minMaxTree.populateTree(this.maxDepth);

            StateTree bestPossibleNext = this.GetBestNextState(this.minMaxTree, ref numExplored);

            if (bestPossibleNext == this.minMaxTree) {
                // There are no next moves, game over
                return null;
            }

            while (this.minMaxTree != bestPossibleNext.Parent) {
                bestPossibleNext = bestPossibleNext.Parent;
            }

            //Console.WriteLine("~~Heuristic Value: {0}", bestPossibleNext.State.Heuristic);
            Move bestNextMove = bestPossibleNext.State.MostRecentMove;
            thoughtTimer.Stop();
            this.report = ("AI: 'It took me " + thoughtTimer.ElapsedMilliseconds + " milliseconds to explore "
                    + numExplored + " choices!' :D" + Environment.NewLine);
            return bestNextMove;
        }

        private StateTree GetBestNextState(StateTree focusTree, ref int explorationCounter) {
            explorationCounter++;
            /*if (0 == (explorationCounter % 1000)) {
                Console.WriteLine(".");
                //if (Console.Out != null) Console.Out.Flush();
            }*/
            if (0 >= focusTree.Children.Count) {
                return focusTree;
            } else if (focusTree.State.IsCheckmate()) {
                return focusTree;
            }

            StateTree bestNext = null;
            
            foreach (var childTree in focusTree.Children) {
                StateTree childBestNext = this.GetBestNextState(childTree, ref explorationCounter);
                if (null == bestNext) {
                    bestNext = childBestNext;
                } else if (Team.White == focusTree.State.CurrentTurn) {
                    if (childBestNext.State.Heuristic > bestNext.State.Heuristic) {
                        bestNext = childBestNext;
                    }
                } else if (Team.Black == focusTree.State.CurrentTurn) {
                    if (childBestNext.State.Heuristic < bestNext.State.Heuristic) {
                        bestNext = childBestNext;
                    }
                }
            }
            return bestNext;
        }

        public Team Team {
            get { return this._myTeam; }
        }

        public void SetAIDifficulty(Difficulty difficultyLevel) {
            this.maxDepth = (int)difficultyLevel;
            //throw new NotImplementedException();
        }

        public string GetReport {
            get { return this.report; }
        }
    }
}
