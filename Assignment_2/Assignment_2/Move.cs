﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2
{
    /// <summary>
    /// Class used to represent a move made on a board.
    /// Contains a "from" and "to" location.
    /// Moves can be performed on GameBoard Objects.
    /// </summary>
    public class Move
    {
        private Location from;
        private Location to;

        public Location From
        {
            get { return this.from; }
        }

        public Location To
        {
            get { return this.to; }
        }

        /// <summary>
        /// Public Constructor for Move class.
        /// </summary>
        /// <param name="from">Location of starting point.</param>
        /// <param name="to">Location of end point of piece.</param>
        public Move(Location from, Location to)
        {
            this.from = from;
            this.to = to;
        }

        /// <summary>
        /// Public Constructor for Move class.
        /// <para> Parses the given string, and constructs a move from it. </para>
        /// </summary>
        /// <param name="moveDescriptionStr"></param>
        public Move(String moveDescriptionStr) {
            int fromX, fromY, toX, toY;
            fromX = (int)(Convert.ToChar(moveDescriptionStr.Substring(0, 1)) - 97);
            fromY = (Convert.ToInt32(moveDescriptionStr.Substring(1, 1)) - 1);
            toX = (int)(Convert.ToChar(moveDescriptionStr.Substring(3, 1)) - 97);
            toY = (Convert.ToInt32(moveDescriptionStr.Substring(4, 1)) - 1);
            this.from = new Location(fromX, fromY);
            this.to = new Location(toX, toY);
            Console.WriteLine("Move Seleced:  " + this.ToString());
        }

        /// <summary>
        /// Method to display test repesentation of a move.
        /// </summary>
        /// <returns>A string in the form (from.x, from.y) - (to.x, to.y)</returns>
        public override string ToString()
        {
            string output = "";
            output += (Char)(Convert.ToInt16('a' + this.From.X));
            output += (this.From.Y + 1);
            output += "-";
            output += (Char)(Convert.ToInt16('a' + this.To.X));
            output += (this.To.Y + 1);
            return output;
        }
    }
}