﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2 {
    public class Player : GamePlayerInterface {

        private GameBoard _gameBoard;
        private Team _myTeam;

        public void LeaveGame() {
            throw new NotImplementedException();
        }

        public void JoinGame(GameBoard boardOfGameToJoin, Team teamToJoin) {
            this._gameBoard = boardOfGameToJoin;
            this._myTeam = teamToJoin;
        }

        public Move ChooseNextMove() {
            string movestring;
            Console.WriteLine("\nPlease enter a move:  ");
            movestring = Console.ReadLine();
            List<Move> moves = _gameBoard.getAllMoves(_myTeam);

            if (0 == moves.Count) {
                return null;
            }

            if (!IsValidString(movestring))
            {
                Console.WriteLine("Try again.....");
                return this.ChooseNextMove();
            }
            Move newMove = new Move(movestring);
            if(!IsValid(newMove)){
                Console.WriteLine("Not a valid move.{0}", Environment.NewLine);
                Console.WriteLine("Valid moves are:{0}\t", Environment.NewLine);
                foreach (Move m in moves)
                {
                    Console.Write("{0}, ", m.ToString());
                }
                return this.ChooseNextMove();
            }
            return newMove;
        }

        private bool IsValidString(string move)
        {
            //remove white space
            move.Replace(" ", "");
            //check length
            return move.Length == 5; 
        }
        private bool IsValid(Move move)
        {
            List<Move> moves = _gameBoard.getAllMoves(_myTeam);
            foreach (Move m in moves)
            {
                if (m.ToString() == move.ToString()) return true;
            }
            return false;
        }

        public Team Team {
            get { return this._myTeam; }
        }
    }
}
