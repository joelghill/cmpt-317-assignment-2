﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2 {
    public class StateTree : IEquatable<StateTree>, IComparable<StateTree> {

        private StateTree _parent;
        private State root;
        private List<StateTree> children;

        /// <summary>
        /// State tree is constructed by adding a single State instance as the root.
        /// </summary>
        /// <param name="state"></param>
        public StateTree(State state) {
            this.root = state;
            this.children = new List<StateTree>();
            this._parent = null;
        }

        /// <summary>
        /// Check to see if tree is leaf node.
        /// </summary>
        /// <returns>True if leaf, false otherwise.</returns>
        public bool isLeaf() {
            return children.Count == 0;
        }

        /// <summary>
        /// Fills tree with successor states until the provided depth is reached.
        /// </summary>
        /// <param name="depth">The desired number of </param>
        public void populateTree(int depth) {
            if (this.root.IsCheckmate()) {
                return;
            } else if (this.root.IsStalemate()) {
                return;
            }
            if (0 >= depth) return;
            foreach (State child in this.root.GetSuccessorStates()) {
                this.children.Add(new StateTree(child));
                //this.children.Sort()
            }

            foreach (StateTree child in this.children) {
                child._parent = this;
                child.populateTree(depth - 1);
            }
        }

        /// <summary>
        /// Recursive solution for printing tree.
        /// </summary>
        /// <returns>String of all child states set to string.</returns>
        public override string ToString() {
            string tree = ("StateTree, prevMove:" + this.State.MostRecentMove + ", value: " + this.State.Heuristic);
            string output = ("StateTree, prevMove: " + this.State.MostRecentMove);
            return tree;
            /*
            string tree = this.root.ToString();

            foreach (StateTree child in this.children) {
                tree = tree + child.ToString();
                tree = tree + "\n";
            }
            return tree;*/
        }

        /// <summary>
        /// Accessor for the tree's state;
        /// </summary>
        public State State {
            get { return this.root; }
        }

        /// <summary>
        /// Accessor for child trees.
        /// </summary>
        public List<StateTree> Children {
            get { return this.children; }
        }

        public StateTree Parent {
            get { return this._parent; }
        }

        public int CompareTo(StateTree other) {
            //if (this.root.CurrentTurn = Team.White) {

            //}
            return this.root.Heuristic.CompareTo(other.root.Heuristic);
        }

        public bool Equals(StateTree other) {
            return this.root.Heuristic.Equals(other.root.Heuristic);
        }
    }
}
