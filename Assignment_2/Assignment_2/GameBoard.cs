﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2 {    
    /// <summary>
    /// Type for representing the active player.
    /// Can be either White or Black.
    /// </summary>
    public enum Team {
        White,
        Black,
        None
    };

    /// <summary>
    /// Game board represented as a boolean array.
    /// True if occupied, false otherwise.
    /// </summary>
    public class GameBoard {
        //width and height stored as ints for easy access
        private int width;
        private int height;

        //win rows
        private int blackWin;
        private int whiteWin;

        //Getters for height and width
        public int Width {
            get { return this.width; }
        }

        public int Height {
            get { return this.height; }
        }

        //Board represented as a 2D array of "Piece" Objects.
        public Piece[,] board;

        //Pieces stored in dictionaries
        private Dictionary<string, Piece> black;
        private Dictionary<string, Piece> white;

        /// <summary>
        /// Constructor for GameBoard class. Accepts parameters to determine size of board.
        /// </summary>
        /// <param name="columns">Number of columns in board (Width, or x dimension).</param>
        /// <param name="rows">Number of rows in board (Height, or y dimension).</param>
        public GameBoard(int columns, int rows) {
            this.board = new Piece[columns, rows];
            this.black = new Dictionary<string, Piece>();
            this.white = new Dictionary<string, Piece>();
            this.width = columns;
            this.height = rows;

            //white must get to top of board
            this.whiteWin = this.Height - 1;

            //black must reach bottom of board
            this.blackWin = 0;

            //fill board with peices.
            this.PopulateBoard();
        }

        /// <summary>
        /// Constructor that accepts another GameBoard instance.
        /// </summary>
        /// <param name="other">Game Board to copy</param>
        public GameBoard(GameBoard other) {
            // All Pieces null
            this.board = new Piece[other.Width, other.Height];
            this.black = new Dictionary<string, Piece>();
            this.white = new Dictionary<string, Piece>();
            this.width = other.Width;
            this.height = other.Height;

            //white must get to top of board
            this.whiteWin = this.Height - 1;

            //black must reach bottom of board
            this.blackWin = 0;
            
            //copy over pieces
            foreach (KeyValuePair<string, Piece> entry in other.white) {
                Piece tmpPiece = entry.Value.Clone();

                this.white.Add(entry.Key, tmpPiece);
            }
            foreach (KeyValuePair<string, Piece> entry in other.black) {
                Piece tmpPiece = entry.Value.Clone();

                this.black.Add(entry.Key, tmpPiece);
            }
            //add all pieces to board
            foreach (Piece p in this.white.Values) {
                this.board[p.X, p.Y] = p;
            }
            foreach (Piece p in this.black.Values) {
                this.board[p.X, p.Y] = p;
            }
        }

        /// <summary>
        /// Method to fill row with pawns. Also Adds peices to appropriate Dictionaries.
        /// </summary>
        /// <param name="row">Index of row to fill.</param>
        /// /// <param name="colour">Colour of peices. white or Black</param>
        private void PopulateRow(int row, Team colour) {
            //Length of row will be size of first dimension in 2D board array
            int lengthRow = this.board.GetUpperBound(0);

            for (int i = 0; i <= lengthRow; i++) {
                Piece newP = new Piece(i, row, colour);
                if (colour == Team.White) {
                    this.white.Add(newP.Id, newP);
                } else {
                    this.black.Add(newP.Id, newP);
                }
                this.board[i, row] = newP;
            }
        }

        /// <summary>
        /// Method to move a Piece object from one location to another.
        /// "From" location is set to null in GameBoard.
        /// "To" is set to the object stored at the "from" location in GameBoard.
        /// </summary>
        /// <param name="move">Move object to be applied to the board.</param>
        /// <returns>True if move was aplied, false otherwise.</returns>
        public bool PerformMove(Move move) {
            if (this.board[move.From.X, move.From.Y] == null) {
                return false;
            }
            //If there is already a peice, remove from appropriate dictionary.
            if (this.board[move.To.X, move.To.Y] != null) {
                if (this.board[move.To.X, move.To.Y].Colour == Team.White) {
                    this.white.Remove(this.board[move.To.X, move.To.Y].Id);
                } else {
                    this.black.Remove(this.board[move.To.X, move.To.Y].Id);
                }
            }
            //replace with reference with the piece at "from" location
            this.board[move.To.X, move.To.Y] = this.board[move.From.X, move.From.Y];
            //update location of that piece
            this.board[move.To.X, move.To.Y].X = move.To.X;
            this.board[move.To.X, move.To.Y].Y = move.To.Y;
            // set reference of "from" peice to null.
            this.board[move.From.X, move.From.Y] = null;
            return true;
        }

        /// <summary>
        /// Method to fill board will black and white pieces
        /// </summary>
        private void PopulateBoard() {
            //Put white in bottom row
            this.PopulateRow(0, Team.White);
            //Put black in top row
            this.PopulateRow(this.board.GetUpperBound(1), Team.Black);
        }

        /// <summary>
        /// Method to return all possible moves for specified team.
        /// </summary>
        /// <param name="team">Team to return moves for.</param>
        /// <returns>List of moves for specified team.</returns>
        public List<Move> getAllMoves(Team team) {
            Dictionary<string, Piece>.ValueCollection pieces;
            List<Move> moves = new List<Move>();

            //Return empty list if no team specified.
            if (team == Team.None) return moves;

            if (team == Team.White) {
                pieces = this.white.Values;
            } else {
                pieces = this.black.Values;
            }
            foreach (Piece peice in pieces) {
                List<Move> newMoves = new List<Move>();
                newMoves = (peice.GetMoves(this));
                moves = moves.Concat(newMoves).ToList();
            }
            return moves;
        }

        /// <summary>
        /// Method to set gameBoard to human readable string.
        /// </summary>
        /// <returns> Grid of W, B, or X to reflect board state</returns>
        public override string ToString() {
            String[] borderUtf8 = { "─", "│", "┌", "┐", "└", "┘", "┬", "├", "┤", "┴", "┼" };
            String[] borderNorm = { "-", "|", "+", "+", "+", "+", "+", "+", "+", "+", "+" };
            String[] borders = borderNorm;
            String[] piecesUtf8 = { "●", "○" };
            String[] piecesNorm = { "W", "B" };
            String[] pieces = piecesNorm;
            if (Console.OutputEncoding.EncodingName == Encoding.UTF8.EncodingName) {
                borders = borderUtf8;
                pieces = piecesUtf8;
            }

            string output = "";
            for (int row = 0; row < this.Height; row++) {
                // Print the border
                output += "  ";
                for (int col = 0; col <= this.Width; col++) {
                    if (col == this.Width) {
                        // At the end
                        if (0 == row) {
                            output += (borders[3] + Environment.NewLine);
                        } else {
                            output += (borders[8] + Environment.NewLine);
                        }
                        continue;
                    } else if (col == 0) {
                        // At the beginning
                        if (0 == row) {
                            output += (borders[2] + borders[0]);
                        } else {
                            output += (borders[7] + borders[0]);
                        }
                        continue;
                    } else {
                        if (0 == row) {
                            output += (borders[6] + borders[0]);
                        } else {
                            output += (borders[10] + borders[0]);
                        }
                    }
                }
                // Print line number
                output += ((this.Height - row) + " ");
                // Print the actual board pieces
                for (int col = 0; col <= this.Width; col++) {
                    if (col == this.Width) {
                        output += (borders[1] + Environment.NewLine);
                        continue;
                    }
                    output += borders[1];
                    if (null == this.board[col, (this.LastRow - row)]) {
                        output += " ";
                    } else if (Team.White == this.board[col, (this.LastRow - row)].Colour) {
                        output += pieces[0];
                    } else {
                        output += pieces[1];
                    }
                }
            }
            // Print final border
            output += "  ";
            for (int col = 0; col <= this.Width; col++) {
                if (col == this.Width) {
                    output += (borders[5] + Environment.NewLine);
                } else if (col == 0) {
                    output += (borders[4] + borders[0]);
                } else {
                    output += (borders[9] + borders[0]);
                }
            }
            // Print colum letter
            output += "  ";
            for (int col = 0; col <= this.Width; col++) {
                if (col == this.Width) {
                    output += Environment.NewLine;
                } else {
                    char letter = (char)(97 + col);
                    output += (" " + letter);
                }
            }
                return output;
        }
        /// <summary>
        /// Method to determine if there is a winner on the board.
        /// </summary>
        /// <returns>White, Black, or None.</returns>
        public Team winner() {
            foreach (Piece piece in this.white.Values) {
                if (piece.Y == this.whiteWin) return Team.White;
            }
            foreach (Piece piece in this.black.Values) {
                if (piece.Y == this.blackWin) return Team.Black;
            }

            return Team.None;
        }

        /// <summary>
        /// Public acccessor for the GameBoard's white pieces.
        /// </summary>
        public List<Piece> WhitePieces {
            get {
                List<Piece> tmpList = new List<Piece>();
                foreach (var tmpDictEntry in this.white) {
                    tmpList.Add(tmpDictEntry.Value);
                }
                return tmpList;
            }
        }

        /// <summary>
        /// Public accessor for the GameBoard's black pieces.
        /// </summary>
        public List<Piece> BlackPieces {
            get {
                List<Piece> tmpList = new List<Piece>();
                foreach (var tmpDictEntry in this.black) {
                    tmpList.Add(tmpDictEntry.Value);
                }
                return tmpList;
            }
        }

        public int LastRow {
            get { return (this.height - 1); }
        }

        public bool IsCheckmate() {
            return (this.winner() != Team.None);
        }

        public bool IsStalemate() {
            if (Team.None != this.winner()) {
                return false;
            } else if ((0 == this.getAllMoves(Team.White).Count)
                    && (0 == this.getAllMoves(Team.Black).Count)) {
                // No possible moves
                return true;
            } else {
                return false;
            }
        }
    }
}
