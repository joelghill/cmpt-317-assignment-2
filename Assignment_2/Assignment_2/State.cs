﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2 {
    public class State {
        private GameBoard board;
        private Double heuristic;
        private Team turn;
        private Move mostRecentMove;

        /// <summary>
        /// Snapshot of relevant information of board.
        /// </summary>
        /// <param name="board">Current GameBoard in this state</param>
        /// <param name="turn">Current active player in this State.</param>
        public State(GameBoard board, Team turn) {
            this.board = board;
            this.turn = turn;
            this.heuristic = this.CalculateHeuristic();
            this.mostRecentMove = null;
        }
        private State(GameBoard board, Team turn, Move transitionMove) {
            this.board = board;
            this.turn = turn;
            this.heuristic = this.CalculateHeuristic();
            this.mostRecentMove = transitionMove;
        }

        /// <summary>
        /// <para> Calculates the state's heuristic based off of the current turn. </para>
        /// <para> This means that if it is White's turn, max is considered to be white, and
        /// so the heuristic increases if the game is better for white. </para>
        /// </summary>
        /// <returns></returns>
        private Double CalculateHeuristic() {
            List<Piece> maxPieces = this.board.WhitePieces;
            List<Piece> minPieces = this.board.BlackPieces;
            // the goal int is the row that the piece is trying to reach
            int maxPieceGoal = this.board.LastRow;
            int minPieceGoal = 0;
            Double tmpHeuristic = 0.0;
            // Increase the heuristic for each of max's pieces.
            foreach (var tmpPiece in maxPieces) {
                if (tmpPiece.Y == maxPieceGoal) {
                    // The piece is in its target row, add 1000.
                    tmpHeuristic += 1000;
                } else {
                    // The piece is on the board, but not at its goal, so is only worth 1
                    tmpHeuristic += 1;
                    //tmpHeuristic += (tmpPiece.Y + 1);
                    //tmpHeuristic += (tmpPiece.Y);
                }
            }
            // Decrease the heuristic for each of min's pieces.
            foreach (var tmpPiece in minPieces) {
                if (tmpPiece.Y == minPieceGoal) {
                    // The piece is in its target row, subtract 1000
                    tmpHeuristic -= 1000;
                } else {
                    // The piece is on the board, but not at its goal, so only subtract 1
                    tmpHeuristic -= 1;
                    //tmpHeuristic -= (this.board.Height - tmpPiece.Y);
                    //tmpHeuristic -= (this.board.LastRow - tmpPiece.Y);
                }
            }
            return tmpHeuristic;
            //throw new NotImplementedException();
        }

        public Double Heuristic {
            get { return this.heuristic; }
        }

        /// <summary>
        /// Method to develope successor states based on the number of available moves.
        /// <para>Only returns successor states of opposing team.</para>
        /// </summary>
        /// <returns>List<State> of all possible states.</returns>
        public List<State> GetSuccessorStates() {

            List<State> successors = new List<State>();
            List<Move> moves = new List<Move>();

            moves = board.getAllMoves(this.turn);
            foreach (Move move in moves) {
                GameBoard newGame = new GameBoard(board);
                newGame.PerformMove(move);
                State newState = new State(newGame, this.otherTurn(), move);
                successors.Add(newState);
            }
            return successors;
        }
        /// <summary>
        /// Helper method to return opposing team.
        /// </summary>
        /// <returns> Opposite team value. None is this Team value not set</returns>
        private Team otherTurn() {
            if (this.turn == Team.White) {
                return Team.Black;
            } else if (this.turn == Team.Black) {
                return Team.White;
            }
            return Team.None;
        }
        public Move MostRecentMove {
            get { return this.mostRecentMove; }
        }
        public Team CurrentTurn {
            get { return this.turn; }
        }

        /// <summary>
        /// Method to check game state for the existence of a winner
        /// </summary>
        /// <returns>True if there is a winner on the board.</returns>
        public bool IsCheckmate(){
            return this.board.winner() != Team.None;
        }
        public bool IsStalemate() {
            return this.board.IsStalemate();
        }
    }
}
