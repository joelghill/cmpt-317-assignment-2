﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2 {
    public interface GamePlayerInterface {
        void LeaveGame();
        void JoinGame(GameBoard boardOfGameToJoin, Team teamToJoin);
        Move ChooseNextMove();

        Team Team {
            get;
        }
    }
}
