﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2
{
    /// <summary>
    /// Struct to represent a location on the gameboard.
    /// <para>Contains x and y coordinates.</para>
    /// </summary>
    public struct Location
    {
        private int x;
        private int y;

        public int X {
            get { return this.x; }
        }
        public int Y {
            get { return this.y; }
        }

        public Location(int x, int y) {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Override of toString method for human readable coordinates.
        /// </summary>
        /// <returns>String representation of the Coordinate: "(x, y)"</returns>
        public override string ToString()
        {
            return ("(" + this.x.ToString() + "," + this.y.ToString() + ")");
        }
    }
    /// <summary>
    /// Class to represent a pawn on the gameboard.
    /// </summary>
    public class Piece
    {
        private Location location; 
        private Team colour;
        private string id;

        //Read only string ID of piece
        public string Id
        {
            get { return this.id; }
        } 

        /// <summary>
        /// Constructor for Peice.
        /// </summary>
        /// <param name="x">X coordinate of peice on gameboard.</param>
        /// <param name="y">Y coordinate of peice on gameboard.</param>
        /// <param name="colour">The colour of piece. White or Black.</param>
        public Piece(int x, int y, Team colour)
        {
            this.location = new Location(x, y);
            this.colour = colour;
            this.setId();
        }

        /// <summary>
        /// X coordinate of peice
        /// </summary>
        public int X
        {
            get { return this.location.X; }
            set { this.location = new Location(value, this.Y); }
        }

        /// <summary>
        /// Y coordinate of Peice
        /// </summary>
        public int Y
        {
            get { return this.location.Y; }
            set { this.location = new Location(this.X, value); }
        }

        /// <summary>
        /// Getter for Peice colour.
        /// </summary>
        public Team Colour
        {
            get { return this.colour; }
        }

        public List<Move> GetMoves(GameBoard game)
        {
            //list of moves to return
            List<Move> moves = new List<Move>();

            //determine movement in y direction for pawn.
            int yMove;
            int newY;
            int newX;
            if (this.colour == Team.Black)
            {
                yMove = -1;
            }
            else
            {
                yMove = 1;
            }
            //set newY coordinate for moves.
            newY = this.Y + yMove;

            //set newX coordinate to piece's current x coord
            newX = this.X;

            //if the new y coordinate is off the board, no possible moves.
            if (newY < 0 || newY > game.LastRow) { return moves; }

            //add straight move to list
            if (null == game.board[newX, newY]) {
                Location tmpFrom = new Location(this.X, this.Y);
                Location tmpTo = new Location(newX, newY);
                moves.Add(new Move(tmpFrom, tmpTo));
            }

            //check for diagonal capture left"
            newX = this.X -1;
            if(newX >= 0 && canCapture(game, newX, newY)){
                Location tmpFrom = new Location(this.X, this.Y);
                Location tmpTo = new Location(newX, newY);
                moves.Add(new Move(tmpFrom, tmpTo));
            }

            //check for diagonal capture to right"
            newX = this.X +1;
            if(newX < game.Width && canCapture(game, newX, newY)){
                Location tmpFrom = new Location(this.X, this.Y);
                Location tmpTo = new Location(newX, newY);
                moves.Add(new Move(tmpFrom, tmpTo));
            }
            //Console.WriteLine("Peice created " + moves.Count.ToString() + "moves.");
            return moves;
        }

        /// <summary>
        /// Method to check a location for the existance of an opposing peice.
        /// </summary>
        /// <param name="game">GameBoard in which to check the validity of the move.</param>
        /// <param name="xcor">X coordinate of the peice to be captured.</param>
        /// <param name="ycor">Y coordinate of the peice to be captured.</param>
        /// <returns>True if peice can be captured, false otherwise.</returns>
        private bool canCapture(GameBoard game, int xcor, int ycor)
        {
            if(game.Width <= xcor 
                || game.Height <= ycor
                || xcor < 0
                || ycor < 0)
            {
                return false;
            }
            if (game.board[xcor, ycor] != null
            && game.board[xcor, ycor].colour != this.Colour)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void setId()
        {
            string i;
            if (this.Colour == Team.White)
            {
                i = "w";
            }
            else
            {
                i = "b";
            }
            this.id = i + this.location.ToString();
        }

        public Piece Clone() {
            Piece newPiece = new Piece(this.X, this.Y, this.Colour);
            newPiece.id = this.id;
            return newPiece;
        }
    }
   
}
