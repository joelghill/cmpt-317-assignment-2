﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2 {
    public class Program {
        static void Main(string[] args) {
            int width;
            int heigth;
            int gameSelection;
            GamePlayerInterface player1;
            GamePlayerInterface player2;
            GamePlayerInterface activePlayer;

            Console.WriteLine("Welcome to Pawned!");
            Console.WriteLine("Please Select a Game Mode:" + Environment.NewLine +
                "1. Human vs. Human" + Environment.NewLine +
                "2. Human vs. AI" + Environment.NewLine +
                "3. AI vs. Human" + Environment.NewLine +
                "4. AI vs. AI" + Environment.NewLine);

            gameSelection = GetGameMode();
            Console.Write(Environment.NewLine + "Enter Width of game board:  ");
            width = GetInt(2, 26);
            Console.Write(Environment.NewLine + "Enter Height of game board:  ");
            heigth = GetInt(2, 26);

            GameBoard board = new GameBoard(width, heigth);

            if (gameSelection == 1) {
                player1 = new Player();
                player2 = new Player();
            } else if (gameSelection == 2) {
                player1 = new Player();
                player2 = new PrimaryAI();
                Difficulty difficulty = GetDifficulty();
                ((PrimaryAI)player2).SetAIDifficulty(difficulty);
            } else if (3 == gameSelection) {
                player1 = new PrimaryAI();
                Difficulty difficulty = GetDifficulty();
                ((PrimaryAI)player1).SetAIDifficulty(difficulty);
                player2 = new Player();
            } else {
                player1 = new PrimaryAI();
                Difficulty difficulty = GetDifficulty();
                ((PrimaryAI)player1).SetAIDifficulty(difficulty);
                player2 = new PrimaryAI();
                difficulty = GetDifficulty();
                ((PrimaryAI)player2).SetAIDifficulty(difficulty);
            }
            player1.JoinGame(board, Team.White);
            activePlayer = player1;
            player2.JoinGame(board, Team.Black);

            Team looser = Team.None;
            bool isGameOver = false;
            Console.Clear();
            Console.WriteLine(board.ToString());
            while (!isGameOver) {
                Console.WriteLine("{0}'s move!", activePlayer.Team);

                Move move = activePlayer.ChooseNextMove();
                if (null == move) {
                    isGameOver = true;
                    looser = activePlayer.Team;
                    break;
                }
                board.PerformMove(move);
                Console.Clear();
                Console.WriteLine(board.ToString());
                if (activePlayer.GetType() == typeof(PrimaryAI)) {
                    Console.WriteLine(activePlayer.Team + "AI Chose " + move.ToString());
                    Console.WriteLine(((PrimaryAI)activePlayer).GetReport); 

                }

                if (activePlayer == player1) {
                    activePlayer = player2;
                } else {
                    activePlayer = player1;
                }
                if (4 == gameSelection) {
                    Console.WriteLine(Environment.NewLine + "Press the anykey for the next AI move...");
                    Console.ReadKey();
                }
                if (board.IsCheckmate() || board.IsStalemate()) {
                    isGameOver = true;
                }
            }
            //write final board state to screen.
            Console.Clear();
            Console.WriteLine(board.ToString());
            if (looser != Team.None) {
                if (Team.White == looser) {
                    Console.WriteLine("############## BLACK WINS #############");
                } else {
                    Console.WriteLine("############## WHITE WINS #############");
                }
            } else if (board.IsStalemate()) {
                Console.WriteLine("##############  TIE GAME  #############");
            } else if (board.winner() == Team.White) {
                Console.WriteLine("############## WHITE WINS #############");
            } else {
                Console.WriteLine("############## BLACK WINS #############");
            }
            Console.WriteLine("Press any key to exit....");
            Console.ReadKey();
        }

        private static Difficulty GetDifficulty() {
            Console.WriteLine(Environment.NewLine + "Select a difficulty setting: " + Environment.NewLine);
            string[] values = Enum.GetNames(typeof(Difficulty));
            string selection = "";
            for (int i = 0; i < values.Length; i++) {
                selection += ((i + 1) + ") " + values[i]);
                if (i == (values.Length - 1)) {
                    selection += "  <-- This WILL kill your computer";
                }
                selection += Environment.NewLine;
            }
            Console.Write(selection + "\t");
            int index = (GetInt(1, 10) - 1);

            var enums = Enum.GetValues(typeof(Difficulty));
            if (index > enums.Length) {
                throw new ArgumentOutOfRangeException();
            }
            Difficulty option = (Difficulty)enums.GetValue(index);
            return option;
        }
        private static int GetGameMode() {
            int selection;
            Console.Write("\n Selection (1, 2, 3, or 4): ");
            selection = GetInt(1, 4);
            if (selection == 4) {
                selection = 4;
                return selection;
            } else if (selection == 3) {
                selection = 3;
                return selection;
            }
            if (selection == 2) {
                selection = 2;
                return selection;
            }
            if (selection == 1) {
                selection = 1;
                return selection;
            }
            //at this point no valid selection, so recall function
            return GetGameMode();
        }

        private static int GetInt(int min, int max) {
            string input = Console.ReadLine();
            int selection = -1;
            try {
                selection = Convert.ToInt32(input);
            } catch (FormatException e) {
                Console.WriteLine("Invalid input. Enter an integer.");
                return GetInt(min, max);
            }
            if (selection < min || selection > max) {
                Console.WriteLine("Invalid input. Try Again.");
                return GetInt(min, max);
            }
            return selection;
        }
    }
}
