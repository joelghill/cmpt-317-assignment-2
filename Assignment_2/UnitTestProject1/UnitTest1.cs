﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment_2;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace UnitTestProject1 {
    [TestClass]
    public class UnitTest1 {
        [TestMethod]
        public void TestMethod1() {
            GameBoard game = new GameBoard(5, 6);
            Assert.AreEqual(game.winner(), Team.None);
            Assert.AreEqual(game.Width, 5);
            Assert.AreEqual(game.Height, 6);
            List<Move> whiteMoves = new List<Move>();
            whiteMoves = game.getAllMoves(Team.White);
            Assert.AreEqual(whiteMoves.Count, 5);
            Console.WriteLine("White's Moves:  ");
            foreach (Move m in whiteMoves) {
                Console.WriteLine(m.ToString());
            }
            Console.WriteLine(game.ToString());
            game.PerformMove(whiteMoves[0]);
            Console.WriteLine(game.ToString());
            Move newMove = new Move(new Location(0, 1), new Location(0, 4));
            Console.WriteLine("Moving pawn to end of board:  " + newMove.ToString());
            game.PerformMove(newMove);
            Console.WriteLine(game.ToString());
            Move attack = new Move(new Location(0, 4), new Location(1, 5));
            game.PerformMove(attack);
            Console.WriteLine(game.ToString());
            Assert.AreEqual(game.BlackPieces.Count, 4);
            Assert.AreEqual(game.winner(), Team.White);
        }
        [TestMethod]
        public void TestMethod2() {
            //make new board
            GameBoard game = new GameBoard(5, 6);
            //make moves on board
            Move newMove = new Move(new Location(0, 0), new Location(0, 4));
            Console.WriteLine("Moving pawn to end of board:  " + newMove.ToString());
            game.PerformMove(newMove);

            Console.WriteLine("game after " + newMove.ToString());
            Console.WriteLine(game.ToString());

            GameBoard clone = new GameBoard(game);
            Assert.AreNotEqual(clone, game);
            Assert.AreEqual(clone.ToString(), game.ToString());

            newMove = new Move(new Location(1, 0), new Location(1, 4));
            clone.PerformMove(newMove);
            Console.WriteLine("clone after " + newMove.ToString());
            Console.WriteLine(clone.ToString());

            Assert.AreNotEqual(clone.ToString(), game.ToString());

            newMove = new Move(new Location(0, 4), new Location(1, 5));
            clone.PerformMove(newMove);
            Assert.AreNotEqual(clone.ToString(), game.ToString());
            Assert.AreEqual(clone.winner(), Team.White);
            Assert.AreEqual(game.winner(), Team.None);
        }
        [TestMethod]
        public void testTree() {
            //make new board
            GameBoard game = new GameBoard(5, 6);
            StateTree tree = new StateTree(new State(game, Team.White));
            tree.populateTree(3);
            Console.WriteLine(tree.ToString());
        }
        [TestMethod]
        public void TestGameboardTostring() {
            Console.OutputEncoding = Encoding.UTF8;
            String testFile = Properties.Resources.GameboardTostringSamples;
            String[] testDelimiters = { "Example: " };
            String[] subTestDelims = { ",", ":"+Environment.NewLine };
            String[] individualTests = testFile.Split(testDelimiters, StringSplitOptions.RemoveEmptyEntries);
            foreach (var testStr in individualTests) {
                String[] testInfo = testStr.Split(subTestDelims, StringSplitOptions.RemoveEmptyEntries);
                int width = Int32.Parse(testInfo[0]);
                int height = Int32.Parse(testInfo[1]);
                GameBoard game = new GameBoard(width, height);
                string output = game.ToString();
                Assert.AreEqual(testInfo[2], output);
            }
        }
        [TestMethod]
        public void TestMoveStrings() {
            Move tmpMove = new Move("a5-b4");
            Assert.AreEqual(0, tmpMove.From.X);
            Assert.AreEqual(4, tmpMove.From.Y);
            Assert.AreEqual(1, tmpMove.To.X);
            Assert.AreEqual(3, tmpMove.To.Y);
            Assert.AreEqual("a5-b4", tmpMove.ToString());
        }
        [TestMethod]
        public void TestAIChooseNext() {
            GameBoard game = new GameBoard(5, 5);
            game.PerformMove(new Move("a1-a2"));
            game.PerformMove(new Move("a2-a3"));
            game.PerformMove(new Move("a3-a4"));
            PrimaryAI gameMaster = new PrimaryAI();
            gameMaster.JoinGame(game, Team.White);
            Console.WriteLine(game.ToString());
            Move next = gameMaster.ChooseNextMove();
            
            Assert.AreEqual("a4-b5", next.ToString());
            game.PerformMove(next);
            Console.WriteLine(game.ToString());
        }
    }
}
